########################## LISCO BIDS_converter ####################
#
# These are the commands to run the Heudiconv docker container that:
# 1- Takes-in DICOMS files from the RAW folder and moves them to the Holy_Folder.
# 2- Converts dicoms into NIFTI format.
# 3- Creates an appropiate BIDS folder.
# 4- Validates the BIDS folder.
# 5- Copy the BIDS folder to the Analysis_Folder.
#
# Some information needs to be modified to indicate where your data is. Then this script
# can be run as a bash file once:
# - Docker has been installed
# - The project_name_heuristic.py file has been changed to match the parameters of your
# experiment.
#
## Original commands from to Gonzalez-García (Ghent University).
## Ortiz-Tudela adapted these lines and turned them into a bash file on 23.01.19
#
#####################################################################
### Input in the study info here #####
#
# Who are you?
user_name=ortiz

# What's the name of your project?
project_name=PIVOTAL/yourtask

# Which subject are you converting?
which_sub=01

# What do you want to do?
copy_from_RAW=1
create_BIDS=1
copy_to_analysis_folder=1

# What's the name of the raw dicoms folder (the one coming from BIC).
raw_folder="Prisma_20220830_140319_yourtask_01"

# Get path to heuristic
heur_folder=/home/$user_name/DATA/2_Analysis_Folder/yourtask/scripts/data_curation

# Get the appropriate heuristic name in case you need
# different ones for different subjects
heu_name='example_heuristic.py'

### Now I will build some names for you ###
# Get the raw folder path
raw_path=/home/$user_name/DATA/RAW/$raw_folder

# Get holy folder path
holy_path=/home/$user_name/DATA/2_Holy_Folder/$project_name

# Get analyses folder path
anal_path=/home/$user_name/DATA/2_Analysis_Folder/yourtask/data

# Get fmap json file names
task_fmap_PA=$holy_path/BIDS/sub-$which_sub/fmap/sub-$which_sub""_acq-yourtask_dir-PA_epi.json

### Create the output folders in case they do not exist ###
# Subject-specific DICOMS fodler
if [ ! -d "$holy_path/dicoms/$which_sub" ];then
	mkdir -p "$holy_path/dicoms/$which_sub"
fi

# BIDS root folder
if [ ! -d "$holy_path/BIDS/sub-$which_sub" ];then
	sudo mkdir -p "$holy_path/BIDS"
fi

#----START THE CONVERSION ------#
### Step 1. COPY DICOMS FROM THE RAW FOLDER TO THE PROJECT FOLDER
if [ $copy_from_RAW -eq 1 ]; then
	if [ ! -d "$holy_path/dicoms/$which_sub/" ];then
		echo "moving data from RAW to Holy_Folder..."
		for seq_code in 01 02 03 04 05 06 07 08 09 10 11 12 13 14
		do
				echo "moving $seq_code""_MR"
				sudo mv -vi $raw_path/*$seq_code""_MR* \
				$holy_path/dicoms/$which_sub/
		done
	else
	echo "Copying requested but dicoms folder already exists. Skipping..."
	fi
else
	echo "Skipping copying dicoms from RAW..."
fi

### Step 2. TAKE IN DICOMS, CONVERT THEM INTO NIFTI AND PUT THEM IN BIDS.
# Within the docker call: 
# Line 2 is the path to the DICOMS
# Line 3 is the path where you want the BIDS
# Line 4 is the path to the heuristic file
# Line 5 and 5 are external variables that are passed into the container.
# Line 6 is what looks for/pulls the heudiconv image
# Line 8 are the subject numbers; they should be separated by commas (e.g., '2', '3', '4', '5', '6', '7', '8' , '9', '10')
# Line 9 is where the heuristic file exists and how it is named
# The rest of the lines do not need changing
if [ $create_BIDS -eq 1 ];then
	if [ ! -d "$holy_path/BIDS/sub-$which_sub/" ];then
		sudo docker run --rm -it \
		-v $holy_path/dicoms/:/data \
		-v $holy_path/BIDS/:/output \
		-v $heur_folder/:/heur_folder \
		-e which_sub \
		-e heu_name \
		nipy/heudiconv:latest \
		-d /data/{subject}/* \
		-s $which_sub \
		-f /heur_folder/$heu_name  \
		-c dcm2niix -b \
		-o /output --minmeta
	else
		echo "Session data (BIDS) already exists; moving on..."
	fi

	## Include "intended for" into fmap json
	if [ $which_ses = "01" ];then
		echo "Including fmap info"
		jq '. + {IntendedFor: [
			"func/sub-01_task-yourtask_run-1_bold.nii.gz",
			"func/sub-01_task-yourtask_run-2_bold.nii.gz",
			"func/sub-01_task-template_bold.nii.gz"
			]} ' $task_fmap_PA > tmp.$$.json && mv -uf tmp.$$.json $task_fmap_PA
	fi
else
	echo "Skipping BIDS creation..."
fi


### Step 4. COPY THE BIDS FOLDER INTO ANALYSES FOLDER FOR ANALYSES
if [ $copy_to_analysis_folder -eq 1 ]; then
	if [ ! -d "$anal_path/BIDS/sub-$which_sub" ];then
			sudo cp -vaur $holy_path/BIDS/ \
			$anal_path/
		else
			echo "Session data (BIDS) already exists in Analysis_Folder; won't copy from Holy Folder..."
		fi
	else
		echo "Skipping copying to Analysis_Folder..."
fi
