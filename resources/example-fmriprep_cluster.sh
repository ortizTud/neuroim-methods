#!/bin/bash
#
#SBATCH --array 01
#SBATCH --partition=general1
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=80
#SBATCH --mem-per-cpu=1024
#SBATCH --time=02:30:00
#SBATCH --no-requeue
#SBATCH --mail-type=ALL
# ------------------------------------------

########################## fmriprep ####################
# 
# This script runs a docker image of fmriPrep on files on BIDS format.
# This script is optimized for Frankfurt's HLR cluster.
#
# Javier Ortiz-Tudela (ortiz-tudela@psych.uni-frankfurt.de)
######################################################
######################################################

echo "Loading singularity..."
spack load singularity@3.5.2

# Which subject do you want to run?
subject=01

# Where do you want fmriprep to store temporary stuff? (It will be deleted at the end)
export TEMP_DIR=/path/to/writable/location/fmriprep_temp/$subject
echo "creating temp directory in"
echo $TEMP_DIR
mkdir -p $TEMP_DIR

# Re-direct some environmental variables to writable locations
export SINGULARITY_TMPDIR=$TEMP_DIR
export SINGULARITY_CACHEDIR=$TEMP_DIR/.cache

# Pass some variables into the container
export SINGULARITYENV_subject=$subject
export SINGULARITYENV_TEMPLATEFLOW_HOME=/templateflow

# Remove 'IsRunning' files from FreeSurfer (just in case it is still running after a crash)
find ${FREESURFER_HOST_CACHE}/$subject/ -name "*IsRunning*" -type f -delete

# Setup done, print a message and run the command
echo -e "\n"
echo "Starting fmriprep."
echo "subject: $subject"
echo -e "\n"

singularity run \
	-B /path/to/data/BIDS:/data:ro \
	-B /path/to/output/folder:/output \
	-B /path/to/templates/templateflow:/templateflow \
	-B /path/to/license:/lic \
	-B $TEMP_DIR:/home/fmriprep \
	-B $TEMP_DIR:/working_dir \
	--home /home/fmriprep --cleanenv \
	/path/to/image/fmriprep-20.1.0.simg \
	/data /output participant \
	--notrack \
	--fs-license-file /lic/license.txt \
	--participant-label $subject \
	--work-dir /working_dir
