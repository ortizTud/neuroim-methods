import os


def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes


def infotodict(seqinfo):
    """Heuristic evaluator for determining which runs belong where

    allowed template
s - follow python string module:

    item: index within category
    subject: participant id
    seqitem: run number during scanning
    subindex: sub index within group
    """

    # Modify this bit to include all the scans that you want to convert into BIDS.
    # This should be the format: VARIABLE_NAME = create_key('PATH/filename')
    t1w = create_key('sub-{subject}/anat/sub-{subject}_acq-orig_T1w')
    hc_highres = create_key('sub-{subject}/anat/sub-{subject}_acq-hcHighres_T1w')
    run1 = create_key('sub-{subject}/func/sub-{subject}_task-yourtask_run-1_bold')
    run2 = create_key('sub-{subject}/func/sub-{subject}_task-yourtask_run-2_bold')
    retmap = create_key('sub-{subject}/func/sub-{subject}_task-retmap_bold')
    distPA = create_key('sub-{subject}/fmap/sub-{subject}_acq-premce_dir-PA_epi')


    # Now also add the new keys also here
    info = {t1w: [],
            hc_highres: [],
            run1: [],
            run2: [],
            run3: [],
            run4: [],
            template: [],
            distPA: [],
          }

    for idx, s in enumerate(seqinfo):
	# Now add here a new "if" statement for the keys that were created above.
        if s.protocol_name == 'MPRAGE_1mm_new':
            info[t1w] = [s.series_id]
        if s.protocol_name == 'lowSAR_v1_PD_tse_0.4_0.4_2.0_TE16':
            info[hc_highres] = [s.series_id]
        if s.protocol_name == 'Mapping_1p5mm_run1':
            if s.series_uid == "1.3.12.2.1107.5.2.43.166058.2022083014520245756614034.0.0.0":
                info[run1] = [s.series_id]
            elif s.series_uid == "1.3.12.2.1107.5.2.43.166058.2022083015083445554915439.0.0.0":
                info[run2] = [s.series_id]
        if s.protocol_name == 'Mapping_1p5mm_ref':
            info[distPA] = [s.series_id]

    return info
