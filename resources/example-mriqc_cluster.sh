#!/bin/bash
#
#SBATCH --array 2
#SBATCH --partition=test
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=80
#SBATCH --mem-per-cpu=1024
#SBATCH --time=00:50:00
#SBATCH --no-requeue
#SBATCH --mail-type=ALL
# ------------------------------------------

echo "Loading singularity..."
spack load singularity@3.5.2

c=0
while read -r cSub
do
        array[$c]=$cSub
        c=$(($c + 1))
done < /scratch/brainimage/ortiz/FeedBES/subject_list.txt

for i in 0
do
        # Array task ID is the subject number
        subject=${array[${SLURM_ARRAY_TASK_ID}+i]}

        # sleep 1-320 seconds
        echo "sleeping a bit..."
        sleep $[ ( $RANDOM / 100 )  + 1 ]s

        # Set up some more variables
        export SINGULARITY_TMPDIR=/scratch/brainimage/ortiz/FeedBES/mriqc_temp/$subject
        export SINGULARITYENV_subject=$subject
        export SINGULARITYENV_TEMPLATEFLOW_HOME=/templateflow
        export TEMP_DIR=/scratch/brainimage/ortiz/FeedBES/mriqc_temp/$subject
        mkdir -p $TEMP_DIR

        # Setup done, run the command
        echo -e "\n"
        echo "Starting mriqc."
        echo "subject: $subject"
        echo -e "\n"

        singularity run \
        -B /scratch/brainimage/ortiz/FeedBES/:/home/mriqc \
        -B /scratch/brainimage/ortiz/FeedBES/BIDS:/data:ro \
        -B /scratch/brainimage/ortiz/templateflow:/templateflow \
        -B /scratch/brainimage/ortiz/FeedBES/BIDS/derivatives/mriqc:/out \
        -B $TEMP_DIR:/working_dir \
        --home /home/mriqc \
        /scratch/brainimage/ortiz/sing_image/mriqc-0.15.2.simg \
        /data /out participant \
        --no-sub \
        --participant_label $subject \
        --work-dir /working_dir

        echo -e "\n"
        echo "Removing temp directory..."
        rm -r $TEMP_DIR

done