########################## fmriprep ####################
# 
# This script runs fmriPrep on files on BIDS format.
#
# Javier Ortiz-Tudela (ortiz-tudela@psych.uni-frankfurt.de)
#####################################################################
#####################################################################
### Input in the study info here #####

# Which subject are you preprocessing?
which_sub=9999

# Where is your project located?
project_name=~/path/to/project/

### Now I will build some names for you ###
# Build BIDS and output folders paths
bids_path=$project_path/BIDS
out_path=$project_path/BIDS/derivatives

# Build license folder path
lic_path=$project_path/

#----START PREPROCESSING ------#
sudo docker run -ti --rm \
-e $which_sub \
-v $bids_path/:/data:ro \
-v $out_path/:/output \
-v $lic_path/:/lic \
poldracklab/fmriprep:latest \
/data /output \
participant \
--participant_label $which_sub \
--fs-license-file /lic/license.txt
