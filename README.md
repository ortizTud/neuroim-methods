[[_TOC_]]
# NeuroIm Methods meetings
These are monthly meetings to discuss methodological papers on fMRI. The level of the topics is very variable (ranging from basic fMRI preprocessing to advance multivariate techniques) since the program is updated dynamically. The materials we use in the meetings will be hosted in this gitlab repo and so is the agenda for the upcoming meetings. We meet on the first Monday of every month (3 p.m. CET). Do contact me if you want to join the meetings via Zoom. 

# How to use this Wiki?
In this Wiki we will document the meetings. On each session, one of the attendees will be selected to create an entry for the topic discussed. In addition, the slides or materials used for that session will also be a part of each session's entry.

If you need info about the scanner booking policy, click [here](https://gitlab.com/ortizTud/neuroim-methods/-/wikis/Scanner-booking-info) (requires gitlab account and access).

# Sessions summary

## 18.11.19 BOLD estimation and jittering _(Korinth, Mayer and Ortiz)._

- Brief summary: 
Planning an fMRI experiment involves a great deal of planning. A big part of this planning stage is committed to obtaining an optimal design (i.e., a distribution of condition and timings that allow for a maximal isolation of the signals of interest). Our design will be good if it allows us to detect our intended effect; it will be better (than others) if it does so more easily; it will be ‘best’ when we give up on finding a better one (that’s what most algorithms do when you set a deadline for their computations).

- Bullet points:
  - Design efficiency refers to the ability/ease to detect a given effect.
  - Design efficiency is NOT related to how well the signal is measured.
  - Design efficiency is always relative.
  - General goal: minimize overlap between desired effects.
  - Efficient designs must be humanly feasible
(physiologically and cognitively; i.e., no torture-like long and no boring-to-death).
  - Trial order and timing are the key weapons.
- Here are a few tools that can be used to maximize the efficiency of our design:
  - Optseq (https://surfer.nmr.mgh.harvard.edu/optseq/)
  - OptimizeX (http://www.bobspunt.com/easy-optimize-x/)
  - Condition-rich designs (Zeithamova et al., 2017)
  - NeuroDesign (http://neuropowertools.org/)
  - Homebrewed scripts (https://tinyurl.com/mumford-efficiency)

- [Slides](https://gitlab.com/ortizTud/neuroim-methods/blob/master/neuroIm_methods_BOLD_jitter.pdf)

- Useful readings:
  - http://imaging.mrc-cbu.cam.ac.uk/imaging/DesignEfficiency
  - https://www.youtube.com/channel/UCZ7gF0zm35FwrFpDND6DWeA
  - https://fmri-training-course.psych.lsa.umich.edu/wp-content/uploads/2017/08/3_Tuesday_Efficiency-Design-Optimization_Mumford2017.pdf
  - Huettel, S. A., Song, A.W., & McCarthy, G. (2008). Functional Magnetic Resonance Imaging (2nd edition). Sinauer Associates, Inc: Sunderland, Massachusetts, USA.
  -https://www.brainvoyager.com/bv/doc/UsersGuide/BrainVoyagerUsersGuide.html

  - Kriegeskorte, N., Mur, M., & Bandettini, P. (2008). Representational similarity analysis - connecting the branches of systems neuroscience. Frontiers in Systems Neuroscience, 2 1–28. https://doi.org/10.3389/neuro.06.004.2008


## 02.12.19 Distortion correction _(Kraft and Ortiz)._

- Brief summary: 
Echo-Planar Images (EPI) is a fast method for acquiring images. In contrast to other types of acquisition methods that can take several minutes, EPI sequences can reconstruct the entire imaging field in seconds. This is the reason why EPI is the most commonly used method in functional fMRI. However, this fast acquisition comes at the cost of large distortions in the phase encoding direction. Although most scanning sequences usually include some form of homogenization of the magnetic field (i.e., shimming) which reduces field inhomogeneity and hence distortions, it is often convinient to apply post-acquisition corrections. The two main methods discussed are 1) field map corrections and 2) blip.

- Bullet points:
   - Distortion are due to differences in density between adjacent tissues (e.g., bone-air, gray matter-air) and therefore are local in nature.
   - The locality of this distortion make corrections difficult.
   - Two main approaches are usually take to correct for these distortions: field map estimation and blip (also know as topup, PEPOLAR or blip-up/blip-down). This first one requires the estimation of the distortions in the magnetic field (about 1-2 minutes); the second one requires the acquisition of a few volumes in the opposite phase encoding direction to the one used for the task EPIs.
   - When possible, blip is generally recommended over field map.

- Useful tools:
  - https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/topup (Topup in FSL)
  - https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FUGUE/Guide (FM in FSL)
  - https://fmriprep.readthedocs.io/en/stable/usage.html (fmriprep SDC)

- [Slides](https://gitlab.com/ortizTud/neuroim-methods/blob/master/neuroIm_methods_distortion.pdf)

- Useful readings:
  - https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/topup/ApplytopupFurtherInformation
  - https://fmriprep.readthedocs.io/en/stable/sdc.html

## 16.12.19 Preprocessing packages _([unguided](https://www.youtube.com/watch?v=brLwlB8j0JQ) discussion)_

- Brief summary: 


- Bullet points:


- Useful tools:

- [Slides]

- Useful readings:
   - https://www.ncbi.nlm.nih.gov/pubmed/30666750
   - [FSL preprocessing](http://www.humanbrainmapping.org/files/2015/Ed%20Materials/FSL_PreProcessing_Pipeline_OHBM15_Jenkinson.pdf)
   - [BrainVoyager preprocessing](http://www.brainvoyager.com/bvqx/doc/UsersGuide/Preprocessing/PreprocessingOfFunctionalData.html)
   - SPM preprocessing: [GUI](https://fmri-training-course.psych.lsa.umich.edu/wp-content/uploads/2017/08/Preprocessing-of-fMRI-Data-in-SPM-12-Lab-1.pdf) and [Code](https://cran.r-project.org/web/packages/spm12r/vignettes/fmri_processing_spm12r.html)

## 11.08.20 GLMdenoise _([unguided](https://www.youtube.com/watch?v=brLwlB8j0JQ) discussion)_

- Brief summary: 


- Bullet points:


- Useful tools:
   - http://kendrickkay.net/GLMdenoise/
   - https://glmdenoise.readthedocs.io/en/latest/index.html

- [Slides]

- Useful readings:
   - https://www.frontiersin.org/articles/10.3389/fnins.2013.00247/full
   - https://www.sciencedirect.com/science/article/pii/S1053811918307626

## 01.02.21 Test-Retest reliability of fMRI _([unguided](https://www.youtube.com/watch?v=brLwlB8j0JQ) discussion)_

- Brief summary: 
Test-Retest reliability of fMRI measurements is a big concern specially for those researchers aiming at relating activity patterns to stable traits (such as anxiety or depression) or to use them as a diagnostic tool which can even inform prognosis. However, poor reliability can also difficult more basic research by lowering the upper bound of correlations with behaviour.

- Bullet points:
   - Elliot's et al.'s (2020) paper draws a rather pesimistic view on the test-retest reliability of fMRI.
   - Good research practice and advance analsysis techniques can significantly improve reliability.


- Useful tools:

- [Slides]

- Useful readings:
   - https://journals.sagepub.com/doi/pdf/10.1177/0956797620916786
   - https://psyarxiv.com/9eaxk
   - https://www.biorxiv.org/content/10.1101/2021.01.04.425305v1

## 01.03.21 Tutorial on fMRIPrep _(Ortiz)_

- Brief summary: 

**Why?**

- State of the art image pre-processing techniques.
- Robust.
- Comparable across studies.
- Easy to use.

**Why not?**

- You are a highly capable engineer (or equivalent) trained on image processing and you disagree with fMRIprep’s steps.
- Very narrow FoVs.
- Populations with non-normative human-brain shapes (e.g., pacients, infants).
- Not knowing how to run it.


- Bullet points:
   - Easy-to-use with containers (Docker or Singularity).
   - Time and resource taxing. It is worth running on the HPC.
   - When running on an HPC, watch for the number of intermidiate files.

- Useful tools:
   - What is fMRIprep? https://fmriprep.org/en/stable/
   - Tutorial and template scripts: https://sites.google.com/view/to-future-me/blog/how-to-fmriprep
 
- [Slides](https://gitlab.com/ortizTud/neuroim-methods/-/blob/master/neuroIm_fmriprep.pptx)

- Useful readings:
  
   - Interact with fMRIPrep: https://fmriprep.org/en/stable/usage.html#command-line-arguments
   - What is BIDS?: https://bids.neuroimaging.io/
   - What is Docker?: https://opensource.com/resources/what-docker
  

# Who are we?
We will use this as a introductory board to outline our areas of expertise/interest. The idea is to have a quick way of knowing what others are working/have worked on so that they can be easily reached if in need. You do not need to be an expert to list yourself here: sometimes sharing even a little experience can safe others a ton of time.

## Javier Ortiz-Tudela (ortiztudela@psych.uni-frankfurt.de)
- MRI quality check (MRIQC).
- Preprocessing (fmriprep). 
- Preprocessing (BrainVoyager).
- Preprocessing (SPM).
- GLM (MATLAB).
- GLM (SPM).
- GLM Least Squares Separate (SPM).
- Parametric modulation of GLM.
- Linear classifiers (MATLAB).
- Linear classifiers (The Decoding Toolbox).
- Linear classifiers (Cosmo MVPA).
- RSA (The Decoding Toolbox).
- RSA (RSA Toolbox).
- PPI (SPM).
- PPI (FSL).
- Beta series correlation (MATLAB).
- Variance partitioning (MATLAB).
